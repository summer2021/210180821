该工程在 **RT-Thread studio** 中进行开发

使用硬件为         ART-PI开发板+柿饼M3	

SUmmer_Project 为工程文件

SUmmer_UI为柿饼派的UI工程



1.工程使用前**先后**在studio中烧录**art_pi_bootloader工程**与**art_pi_wifi工程**

在art_pi_wifi工程中连接终端，使用wifi scan扫描周围wifi信号，使用 wifi join <wifi 名称>  <wifi 密码> (只输入wifi名称和密码即可，无需输入"<>")，连接wifi

2.烧录SUmmer_Project 工程文件，数据自动上传至本人onenet平台，可在RT-Thread Settings中的onenet详细配置中修改为自己的onenet云平台。



AI文件夹下为将模型部署到嵌入式平台的文件，详情使用查看RT-AK快速上手

Model_file 为本工程模型训练文件以及数据集。

