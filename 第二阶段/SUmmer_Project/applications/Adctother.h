/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-24     years       the first version
 */
#ifndef APPLICATIONS_ADCTOTHER_H_
#define APPLICATIONS_ADCTOTHER_H_


int adc_getValue(void);



#endif /* APPLICATIONS_ADCTOTHER_H_ */
