/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-24     years       the first version
 */
#ifndef APPLICATIONS_FILE_H_
#define APPLICATIONS_FILE_H_
#include "drv_common.h"


void app_creat();
void app_write(rt_uint16_t id,float *arr,rt_uint16_t lab);
rt_uint8_t app_read(void);


#endif /* APPLICATIONS_FILE_H_ */
