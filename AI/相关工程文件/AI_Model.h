/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-09-24     years       the first version
 */
#ifndef APPLICATIONS_AI_MODEL_H_
#define APPLICATIONS_AI_MODEL_H_




rt_uint8_t AI_app(void *arr);


#endif /* APPLICATIONS_AI_MODEL_H_ */
