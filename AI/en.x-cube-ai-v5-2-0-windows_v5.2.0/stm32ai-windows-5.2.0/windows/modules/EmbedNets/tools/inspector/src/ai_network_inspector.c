/**
  ******************************************************************************
  * @file    ai_network_inspector.c
  * @author  AST Embedded Analytics Research Platform
  * @date    6-Aug-2018
  * @brief   implementation file of the network inspector wrapper plugin
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#include <stdio.h>
#include <string.h>

#include "ai_network_inspector.h"
#include "ai_platform_interface.h"

#include "ai_memory_manager.h"

#include "core_lists.h"
#include "core_net_inspect.h"

//#define AI_ACTIVATE_NET_LOG      LOG_TRACE

#define AI_INSPECTOR_OBJ(obj_) \
  ((ai_inspector_context*)(obj_))

#define AI_INSPECTOR_ACQUIRE_CTX(handle_) \
  AI_INSPECTOR_OBJ(ai_platform_context_acquire(handle_))

#define AI_INSPECTOR_RELEASE_CTX(ctx_) \
  AI_HANDLE_PTR(ai_platform_context_release(AI_CONTEXT_OBJ(ctx_)))

#define PRINT_BUFFER(msg, buf) \
  AI_LOG_INFO(msg "%s (%d, %d, %d) n_batches: %d data: %p" AI_CR, \
      AI_BUFFER_FMT_NAME((buf)->format), \
      (buf)->width, (buf)->height, (buf)->channels, \
      (buf)->n_batches, (buf)->data )


/*!
 * @struct ai_inspector_entry
 * @ingroup ai_network_inspector
 * @brief list item storing bound network context (i.e. net_entry, net_id)
 */
typedef struct ai_inspector_entry_ {
  ai_inspector_net_entry       net_entry; /*!< bound net_entry struct */
  ai_inspector_entry_id        net_id;    /*!< bound network identification index */
  ai_dlist                     list;      /*!< @ref ai_dlist datastructure */
} ai_inspector_entry;

/*!
 * @struct ai_inspector_context
 * @ingroup ai_network_inspector
 * @brief global network context with config, bound network list and net_id 
 * global counter
 */
typedef struct ai_inspector_context_s {
  AI_CONTEXT_FIELDS
  ai_inspector_config       config;      /*!< inspector context configuration */
  ai_dlist                  entry_list;  /*!< double linked list of bound networks */
  ai_inspector_entry_id     net_id;      /*!< bound network index */
} ai_inspector_context;


/*!< global inspector context instance */
AI_STATIC
ai_inspector_context g_inspector_context = AI_STRUCT_INIT;


/*!
 * @brief Internal routine to inspect a bound network
 * @ingroup ai_network_inspector
 * @param[in/out] ctx a pointer to the inspector module context 
 * @param[in] net_entry a pointer to the net_entry struct with all info about
 * the bound network
 * @return true if network inspection initialization is OK, false otherwise 
 */
AI_DECLARE_STATIC
ai_bool _inspector_network_inspect_init(
  ai_inspector_context* ctx, const ai_inspector_net_entry* net_entry)
{
  AI_ASSERT(ctx && net_entry)

  /* initialize the instance */
  AI_LOG_TRACE("[_inspector_network_inspect_init]: inspecting network (%p)...", 
               net_entry->handle)

  return ai_network_inspect_init(net_entry->handle, &ctx->config);
}

/*!
 * @brief Internal routine to destroy a bound network context
 * @ingroup ai_network_inspector
 * @param[in] net_entry a pointer to the net_entry struct with all info about
 * the bound network to destroy
 * @return true if network inspection destroing is OK, false otherwise 
 */
AI_DECLARE_STATIC
ai_bool _inspector_network_inspect_destroy(
  const ai_inspector_net_entry* net_entry)
{
  AI_ASSERT(net_entry)

  /* initialize the instance */
  AI_LOG_TRACE("[_inspector_network_inspect_destroy]: destroying network (%p)...", 
               net_entry->handle)

  return ai_network_inspect_destroy(net_entry->handle);
}

/*!
 * @brief Internal routine initialize main inspector module context
 * @ingroup ai_network_inspector
 * @param[out] ctx a pointer to the context to initialize
 * @return a pointer to the initialized context 
 */
AI_DECLARE_STATIC
ai_inspector_context* _inspector_context_init(
  ai_inspector_context* ctx)
{
  AI_ASSERT(ctx)

  const ai_inspector_context cleared = AI_STRUCT_INIT;
  *ctx = cleared;

  ctx->config.log_quiet = true;
  ctx->net_id           = AI_INSPECTOR_NETWORK_BIND_FAILED + 1;

#ifdef HAS_LOG
  ctx->config.log_level = LOG_ERROR;
#endif

  AI_DLIST_INIT(&ctx->entry_list)

  return ctx;
}

/*!
 * @brief Internal routine to find in the list of bound network the one with 
 * given id value
 * @ingroup ai_network_inspector
 * @param[in] head a pointer to the list head struct of the bound networks
 * @param[in] the id index of the required network
 * @return a pointer to the net_entry if found in list, NULL otherwise
 */
AI_DECLARE_STATIC
ai_inspector_entry* _inspector_find_entry(
  ai_dlist* head, const ai_inspector_entry_id id)
{
  AI_ASSERT(head)

  AI_DLIST_FOR_EACH_OBJ_NEXT(entry, head, ai_inspector_entry, list)
  {
    if ( entry->net_id == id ) {
      AI_LOG_TRACE("network id %d (%p) found in list_size(%d)",
        entry->net_id, entry->net_entry.handle, AI_DLIST_SIZE(head))
      return entry;
    }
  }

  AI_LOG_WARN("network id %d NOT found in list, size(%d)",
    id, AI_DLIST_SIZE(head))

  return NULL;
}

/*!
 * @brief Internal routine to add a new net_entry in the bound networks list
 * @ingroup ai_network_inspector
 * @param[out] ctx a pointer to the global inspector context
 * @param[in] net_entry a pointer the the entry to add to the list
 * @return a pointer the the entry stored in the list, NULL if failed to allocate
 * the item in the list
 */
AI_DECLARE_STATIC
ai_inspector_entry* _inspector_add_entry(
  ai_inspector_context* ctx, const ai_inspector_net_entry* net_entry)
{
  AI_ASSERT(ctx && net_entry)

  ai_inspector_entry* slot = AI_MEM_ALLOC(1, ai_inspector_entry);
  if ( !slot ) {
    return NULL;
  }

  slot->net_id = ctx->net_id++;
  slot->net_entry = *net_entry;
  AI_DLIST_INIT(&slot->list)

  AI_DLIST_PUSH_BACK(&slot->list, &ctx->entry_list)

  AI_LOG_TRACE("[_inspector_add_entry] network id %d (%p) inserted in list, \
    size(%d)",
    slot->net_id, slot->net_entry.handle, AI_DLIST_SIZE(&ctx->entry_list) )

  return slot;
}

/*!
 * @brief Internal routine to remove (unbind) a network entry from the bound
 * networks list
 * @ingroup ai_network_inspector
 * @param[out] ctx a pointer to the global inspector context
 * @param[in] entry a pointer the the entry in the list to be removed
 * @param[in] force_removal_on_failure flag that forces the removal from the list
 * even when the inspect destroy routine fails to unbind the network
 * @return true if the unbind is OK, false otherwise
 */
AI_DECLARE_STATIC
ai_bool _inspector_unbind_network(
  ai_inspector_context* ctx, ai_inspector_entry* entry,
  const ai_bool force_removal_on_failure)
{
  AI_ASSERT(ctx && entry)

  if ( _inspector_network_inspect_destroy(&entry->net_entry) ) {
    /* Do the unbind */

    AI_DLIST_OBJ_REMOVE(entry, list)
    AI_LOG_TRACE("[_inspector_unbind_network] network id %d (%p) \
      unbind done from list, size(%d)", 
      entry->net_id, entry->net_entry.handle, AI_DLIST_SIZE(&ctx->entry_list))

    AI_MEM_FREE(entry)

    return true;
  }

  if ( force_removal_on_failure ) {
    AI_DLIST_OBJ_REMOVE(entry, list)

    AI_LOG_WARN("[_inspector_unbind_network] forced removal of network %d (%p) \
      from list, size(%d)",
      entry->net_id, entry->net_entry.handle, AI_DLIST_SIZE(&ctx->entry_list))
    
    AI_MEM_FREE(entry)
  }

  return false;
} 

/********* PUBLIC APIs INTERFACE **********************************************/

AI_API_ENTRY
ai_inspector_config ai_inspector_default_config(void)
{
  ai_inspector_config out = AI_STRUCT_INIT;
  out.validation_mode |= VALIDATION_INSPECT;
  return out;
}

AI_API_ENTRY
ai_bool ai_inspector_create(
  ai_handle* handle, const ai_inspector_config* cfg)
{
  if ( !handle ) return false;
 
  ai_inspector_context* ctx = _inspector_context_init(&g_inspector_context);
  if ( !ctx ) {
    *handle = AI_HANDLE_NULL;
    AI_LOG_FATAL("[ai_inspector_create] Failed to create context")
    return false;
  }

  ctx->config = (cfg) ? *cfg : ai_inspector_default_config();

  AI_LOG_SET_LEVEL(ctx->config.log_level)
  AI_LOG_SET_QUIET(ctx->config.log_quiet)

#ifdef AI_ACTIVATE_NET_LOG
  AI_LOG_SET_LEVEL(AI_ACTIVATE_NET_LOG)
  AI_LOG_SET_QUIET(false)
#endif
  
  *handle = AI_INSPECTOR_RELEASE_CTX(ctx);

  AI_LOG_TRACE("[ai_inspector_create] context(%p) created.", ctx)

  return true;
}

AI_API_ENTRY
ai_bool ai_inspector_destroy(ai_handle handle)
{
  ai_inspector_context* ctx = AI_INSPECTOR_ACQUIRE_CTX(handle);
  if ( !ctx ) {
    AI_LOG_ERROR("[ai_inspector_destroy] Failed to acquire context")
    return false;
  }

  AI_LOG_TRACE("[ai_inspector_destroy] ctx(%p) Trying to deinit %d networks",
    ctx, AI_DLIST_SIZE(&ctx->entry_list))

  AI_DLIST_FOR_EACH_OBJ_NEXT_SAFE(entry, &ctx->entry_list, ai_inspector_entry, list)
  {
    AI_LOG_WARN("[ai_inspector_destroy] Forgot to unbind network id %d (%p)?",
      entry->net_id, entry->net_entry.handle)

    if ( !_inspector_unbind_network(ctx, entry, true) ) {
      AI_LOG_WARN("[ai_inspector_destroy] : Failed to unbind network ")
    }
  }

  AI_LOG_TRACE("[ai_inspector_destroy] list, size(%d)",
    AI_DLIST_SIZE(&ctx->entry_list)); 

  return true;
}

AI_API_ENTRY
ai_inspector_entry_id ai_inspector_bind_network(
  ai_handle handle, const ai_inspector_net_entry* net_entry)
{
  ai_inspector_context* ctx = AI_INSPECTOR_ACQUIRE_CTX(handle);
  if ( !(ctx && net_entry && net_entry->handle) ) {
    AI_LOG_WARN("[ai_inspector_bind_network] Invalid parameters provided")
    return AI_INSPECTOR_NETWORK_BIND_FAILED;
  }
  AI_LOG_TRACE("[ai_inspector_bind_network] ctx(%p) Trying to bind network(%p)",
    ctx, net_entry->handle)

  ai_inspector_entry* entry = _inspector_add_entry(ctx, net_entry);
  if ( !entry ) return AI_INSPECTOR_NETWORK_BIND_FAILED;

  AI_LOG_TRACE("[ai_inspector_bind_network] Network(%p) inserted with id(%d)",
    entry->net_entry.handle, entry->net_id);

  if ( _inspector_network_inspect_init(ctx, &entry->net_entry) )
  {
    AI_LOG_SET_LEVEL(ctx->config.log_level)
    AI_LOG_SET_QUIET(ctx->config.log_quiet)

    AI_LOG_TRACE("Network binding done id(%d)", entry->net_id)

#ifdef AI_ACTIVATE_NET_LOG
    AI_LOG_SET_LEVEL(AI_ACTIVATE_NET_LOG)
    AI_LOG_SET_QUIET(false)
#endif

    return entry->net_id;
  }

  AI_LOG_ERROR("[ai_inspector_bind_network] Failed to bind network (%p)",
    entry->net_entry.handle)
  AI_DLIST_OBJ_REMOVE(entry, list)
  AI_MEM_FREE(entry)
  ctx->net_id--;

  return AI_INSPECTOR_NETWORK_BIND_FAILED;
}

AI_API_ENTRY
ai_bool ai_inspector_unbind_network(
  ai_handle handle, const ai_inspector_entry_id net_id)
{
  ai_inspector_context* ctx = AI_INSPECTOR_ACQUIRE_CTX(handle);
  if ( !(ctx && net_id>AI_INSPECTOR_NETWORK_BIND_FAILED) ) {
    AI_LOG_WARN("[ai_inspector_unbind_network] Invalid parameters provided")
    return false;
  }

  AI_LOG_TRACE("[ai_inspector_unbind_network] ctx(%p) trying to unbind \
    network id %d...",
    ctx, net_id)

  ai_inspector_entry* entry;

  if ((entry=_inspector_find_entry(&ctx->entry_list, net_id)) != AI_HANDLE_NULL) {

    AI_LOG_TRACE("unbind found network id %d (%p) el(%d)\r\n", 
      entry->net_id, entry->net_entry.handle, AI_DLIST_SIZE(&ctx->entry_list));  

    return _inspector_unbind_network(ctx, entry, false);
  }

  AI_LOG_ERROR("[ai_inspector_unbind_network] Failed to unbind net_id(%u). \
    Entry not found",
    net_id)

  return false;
}

AI_API_ENTRY
ai_bool ai_inspector_get_report(
  ai_handle handle,
  const ai_inspector_entry_id net_id,
  ai_inspector_net_report* report)
{
  ai_inspector_context* ctx = AI_INSPECTOR_ACQUIRE_CTX(handle);
  if ( !(ctx && report) ) {
    AI_LOG_WARN("[ai_inspector_get_report] Invalid parameters")
    return false;
  }

  AI_LOG_TRACE("[ai_inspector_get_report] net_id(%u) ctx(%p) report(%p)",
    net_id, ctx, report)

  ai_inspector_entry* entry = NULL;

  if ((entry=_inspector_find_entry(&ctx->entry_list, net_id)) != AI_HANDLE_NULL) {
    return ai_network_inspect_get_report(entry->net_entry.handle, report);
  }

  AI_LOG_ERROR("[ai_inspector_get_report] No network entry %d found", net_id)

  return false;
}

AI_API_ENTRY
ai_i32 ai_inspector_run(
  ai_handle handle,
  const ai_inspector_entry_id net_id, 
  const ai_buffer* input,
  ai_buffer* output)
{
  ai_inspector_context* ctx = AI_INSPECTOR_ACQUIRE_CTX(handle);
  if ( !(ctx && input) ) {
    AI_LOG_WARN("[ai_inspector_run] Invalid parameters")
    return 0;
  }
  ai_inspector_entry* entry = NULL;

  if ((entry=_inspector_find_entry(&ctx->entry_list, net_id)) != AI_HANDLE_NULL)
  {
    AI_LOG_TRACE("[ai_inspector_run] Running network %d (%p)",
      entry->net_id, entry->net_entry.handle)
    return ai_platform_network_process(entry->net_entry.handle, input, output);
  }

  AI_LOG_ERROR("[ai_inspector_run] No network entry %d found", net_id)
  return 0;
}

#undef PRINT_BUFFER
#undef AI_INSPECTOR_OBJ
#undef AI_INSPECTOR_ACQUIRE_CTX
#undef AI_INSPECTOR_RELEASE_CTX
