#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "ai_network_inspector.h"

#include "network.h"
#include "network_data.h"

static
void print_buffers(
  const char* msg, const ai_buffer* buffer, const ai_u16 count,
  const ai_bool dump_data)
{
  if ( !buffer ) return;

  printf("%s\r\n", msg);
  for ( ai_u16 i=0; i<count; i++ ) {
    const ai_size bsize = buffer[i].width * buffer[i].height * buffer[i].channels;
    const ai_float* ptr = (dump_data) ? (ai_float*)buffer[i].data : NULL;

    printf("\t%u/%u : [fmt(0x%x) batches(%u) width(%u) height(%u) channels(%u) data(0x%p)]\r\n",
      i+1, count,
      buffer[i].format, buffer[i].n_batches,
      buffer[i].width, buffer[i].height, buffer[i].channels, buffer[i].data);

    for ( ai_size b=0; ptr && (b<buffer[i].n_batches*bsize); b+=bsize ) {
      printf("\t[ ");
      for ( ai_size i=b; i<b+bsize; i++ ) {
        printf("%f,", ptr[i]);
      }
      printf(" ]\r\n");
    }
  }
  printf("\r\n");
}

static
void network_inspect_print_report( const ai_inspector_net_report* report)
{
  printf("Network inspect report\r\n\n");
  printf("    Report id           : %d\r\n", report->id);
  printf("    Report signature    : 0x%08x\r\n", report->signature);
  printf("    Num inferences      : %u\r\n", report->num_inferences);
  printf("    Network nodes       : %u\r\n", report->n_nodes);
  printf("    Elapsed time (ms)   : %f\r\n", report->elapsed_ms);
  printf("\r\n");

  for ( ai_size i=0; i<report->n_nodes; i++ ) {
    ai_inspect_node_info* node = &report->node[i];
    printf("[node #%3u] info:\r\n", node->id);
    printf("      Type              : 0x%x\r\n", node->type);
    printf("      Elapsed time (ms) : %f\r\n", node->elapsed_ms);
    print_buffers("      Input buffers", node->in, node->in_size, false);
    print_buffers("     Output buffers", node->out, node->out_size, false);
  }
  printf("\r\n");
}


static
void network_print_report(const ai_network_report* report)
{
  printf("Model name        :  %s\r\n", report->model_name);
  printf("Model signature   :  %s\r\n", report->model_signature);
  printf("Model datetime    :  %s\r\n", report->model_datetime);
  printf("\r\n");
  printf("Compile datetime  :  %s\r\n", report->compile_datetime);
  printf("\r\n");
  printf("Runtime revision  :  %s\r\n", report->runtime_revision);
  printf("Runtime version   :  (%d.%d.%d)\r\n",
    report->runtime_version.major,
    report->runtime_version.minor,
    report->runtime_version.micro);
  printf("\r\n");
  printf("Tool revision     :  %s\r\n", report->tool_revision);
  printf("Tool version      :  (%d.%d.%d)\r\n",
    report->tool_version.major,
    report->tool_version.minor,
    report->tool_version.micro);
  printf("Tool APIs version :  (%d.%d.%d)\r\n",
    report->tool_api_version.major,
    report->tool_api_version.minor,
    report->tool_api_version.micro);
  printf("Platform APIs version   :  (%d.%d.%d)\r\n",
    report->api_version.major,
    report->api_version.minor,
    report->api_version.micro);
  printf("Platform interface APIs version   :  (%d.%d.%d)\r\n",
    report->interface_api_version.major,
    report->interface_api_version.minor,
    report->interface_api_version.micro);
  printf("\r\n");
  printf("Network signature  : 0x%x\r\n", report->signature);
  printf("Network nodes      : %d\r\n", report->n_nodes);
  printf("Network complexity : %d\r\n", report->n_macc);
  printf("Network inputs     : %u\r\n", report->n_inputs);
  printf("Network outputs    : %u\r\n", report->n_outputs);
  printf("\r\n");
  print_buffers("Network input      buffers", report->inputs, report->n_inputs, false);
  print_buffers("Network output     buffers", report->outputs, report->n_outputs, false);
  print_buffers("Network activation buffer", &report->activations, 1, false);
  print_buffers("Network params     buffer", &report->params, 1, false);
  printf("\r\n");
}

static
ai_network_params get_network_params(const ai_handle activations)
{
  const ai_network_params params = {
    AI_NETWORK_DATA_WEIGHTS(ai_network_data_weights_get()),
    AI_NETWORK_DATA_ACTIVATIONS(activations) };
  return params;
}

static
ai_handle network_bootstrap(const ai_handle activations)
{
  ai_handle network = AI_HANDLE_NULL;
  ai_network_report report;
  ai_network_params params;

  /* Create the network instance */
  ai_error error = ai_network_create(&network, AI_NETWORK_DATA_CONFIG);
  if ( AI_ERROR_NONE!=error.type ) {
    printf("Network creation failed with error 0x%x code: 0x%x\n",
      error.type, error.code);
    return AI_HANDLE_NULL;
  }

  /* Query the created network to get relevant info from it */
  if ( ai_network_get_info(network, &report) ) {
    network_print_report(&report);
  } 

  // Retrieve network params formatting them
  params = get_network_params(activations);

  /* Initialize the network providing its params */
  if ( !ai_network_init(network, &params) ) {
    /* In case of failure query the error */
    error = ai_network_get_error(network);
    printf("Network initialization failed with error 0x%x code: 0x%x\n",
      error.type, error.code);
    /* destroy the network */
    ai_network_destroy(network);
    return AI_HANDLE_NULL;
  }

  /* return an handle to the network instance created */
  return network;
}

static 
ai_i32 network_process_batch(
  ai_handle network, const ai_buffer* input, ai_buffer* output)
{
  if ( !(network && input && output) ) return 0;

  return ai_network_run(network, input, output);
}

static
ai_handle network_shutdown(ai_handle network)
{
  /* Destroy the network instance
   * NB: if OK it should return AI_HANDLE_NULL value */
  return ai_network_destroy(network);  
}

/* fill a float array with random data in the range [-1, 1] */
static 
void fill_random(ai_float* data, const ai_size size)
{
  for ( ai_size i=0; i<size; i++ ) {
    const ai_float r = (((ai_float)rand()/(ai_float)RAND_MAX)*2)-1;
    data[i] = r;
  }
}

#define NUM_BATCHES         (10)

/*
static
void inspector_node_get(
  const ai_handle cookie,
  const ai_inspect_node_info* node_info,
  ai_node_exec_stage stage)
{

}
*/

int main(int argc, char* argv[])
{
  ai_handle inspector = AI_HANDLE_NULL;
  ai_inspector_config cfg = ai_inspector_default_config();
  ai_u16 i;

  /* Static memory reserved for network allocations */
#if (AI_NETWORK_DATA_ACTIVATIONS_SIZE>0)
  ai_u8 network_activations[AI_NETWORK_DATA_ACTIVATIONS_SIZE];
#else
  ai_u8 network_activations[] = AI_C_ARRAY_INIT;
#endif

  /* Network input and outputs data arrays */
  const ai_size in_size[] = AI_NETWORK_IN_SIZE;
  const ai_size out_size[] = AI_NETWORK_OUT_SIZE;
  ai_float input_data[ NUM_BATCHES * in_size[0] ];
  ai_float output_data[ NUM_BATCHES * out_size[0] ];
  ai_i32 batches_processed = 0;

  /* Inputs / outputs buffer declaration */
  ai_buffer inputs[AI_NETWORK_IN_NUM] = AI_NETWORK_IN;
  ai_buffer outputs[AI_NETWORK_OUT_NUM] = AI_NETWORK_OUT;

  ai_handle network = network_bootstrap(AI_HANDLE_PTR(network_activations));
  if ( !network ) {
    return -1;
  }

  ai_inspector_entry_id net_id = AI_INSPECTOR_NETWORK_BIND_FAILED;
  ai_inspector_net_report inspector_report;

  //cfg.on_exec_node = inspector_node_get;
  cfg.validation_mode |= (VALIDATION_INSPECT|STORE_ALL_IO_ACTIVATIONS);
  srand(time(NULL));

  if ( ai_inspector_create(&inspector, &cfg) )
  {
    ai_inspector_net_entry entry;
    entry.handle = network;
    entry.params = get_network_params(AI_HANDLE_PTR(network_activations));
    net_id = ai_inspector_bind_network(inspector, &entry);
    if ( net_id == AI_INSPECTOR_NETWORK_BIND_FAILED ) {
      printf("Failed to bind network(%p) to inspector(%p)\r\n",
        network, inspector);
    } else {
      printf("Network (%p) bind to inspector(%p)\r\n", network, inspector);
      printf("\r\n\n");
    }
  }

  /* Set the number of batches to process and bind their buffers */
  for ( i=0; i<AI_NETWORK_IN_NUM; i++ ) {
    inputs[i].n_batches  = NUM_BATCHES;
    inputs[i].data = AI_HANDLE_PTR(input_data);
    AI_ASSERT(inputs[i].data && (in_size[i]>0))

    /* Fill with random numbers input */
    fill_random(inputs[i].data, NUM_BATCHES * in_size[i]);
  }
  for ( i=0; i<AI_NETWORK_OUT_NUM; i++ ) {
    outputs[i].n_batches = NUM_BATCHES;
    outputs[i].data = AI_HANDLE_PTR(output_data);
    AI_ASSERT(outputs[i].data && (out_size[i]>0))
  }

  /* Do the processing! */
  for ( ai_size i=0; i<3; i++ ) {
    batches_processed = network_process_batch(network, inputs, outputs);
    printf("Processed %d batches\r\n", batches_processed);


    if ( (batches_processed==NUM_BATCHES) && 
         ai_inspector_get_report(inspector, net_id, &inspector_report) ) {
      network_inspect_print_report(&inspector_report);
      print_buffers("     Output Results", outputs, 1, true);
      printf("\r\n\r\n");
    }
  }

  network = network_shutdown(network);

  if ( inspector && (net_id!=AI_INSPECTOR_NETWORK_BIND_FAILED) ) {
    ai_inspector_unbind_network(inspector, net_id);
    ai_inspector_destroy(inspector);
  }

  return 0;
}
