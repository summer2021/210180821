#!/bin/bash
_pwd="$(pwd)"
_make_cmd="make"

case $OSTYPE in
  'msys')
    _make_cmd="mingw32-make"
    ;;
  *) ;;
esac

${_make_cmd} clean && ${_make_cmd} copy_generated_files && ${_make_cmd} install

cd ${_pwd}