#!/usr/bin/env python
# -*- coding: utf8 -*-
# pylint: disable=too-few-public-methods, too-many-instance-attributes
"""
=========================================
Python Wrappers for Inspector and Network
=========================================

This module exposes CTypes interfaces for the network C API (in *libai_network*
) and for the inspector C API (in *libai_inspector*).

The Network class can load the network defined in the library code, load
weights from the pre-defined *network_data.c* and *network_data.h* modules or
from an user-provided Numpy array and run the network on batched input tensors
in AiBuffer format.

The Inspector class loads the predefined network library (*libai_network*) and
bind it to the inspector module; the user then can run the library on batched
input data in Numpy format.
Depending on the inspection mode selected in initialization, the inspection
report will have dumps of the intermediate features or activation statistics.
The library handles the memory of the report, so you need to copy the data
before the inspector goes out of scope to avoid segfaults.
"""
from __future__ import division
from __future__ import print_function

import ctypes as ct
import enum
import logging
import os

import numpy as np
import numpy.ctypeslib as npct   # simplifies cross-platform CTypes

__author__ = "AST Embedded Analytics Research group"
__copyright__ = "Copyright (c) 2018, STMicroelectronics"
__license__ = "New BSD License - http://opensource.org/licenses/BSD-3-Clause"


LIB_PATH = os.path.join(os.path.dirname(__file__), "workspace/lib")


class Printable(ct.Structure):
    """C struct interface for printable field contents; used for debug and
    logging."""

    def __str__(self):
        """Print the content of the structure."""
        msg = "[{}]".format(self.__class__.__name__)

        for field in self._fields_:
            member = getattr(self, field[0])
            # parse pointer addresses
            try:
                if hasattr(member, 'contents'):
                    member = hex(ct.addressof(member.contents))
            except ValueError:  # NULL pointer
                member = "NULL"

            indented = "\n".join(
                "  " + line for line in str(member).splitlines())
            msg += "\n  {}:{}".format(field[0], indented)

        return msg + "\n"


def _wrap_api(lib, funcname, restype, argtypes):
    """Simplify wrapping CTypes functions."""
    func = getattr(lib, funcname)
    func.restype = restype
    func.argtypes = argtypes
    return func


#
# Network wrapper
#

class AiInspectMode(enum.IntEnum):
    """Inspection mode."""
    VALIDATION_INSPECT = (0x1 << 0)
    STORE_ALL_IO_ACTIVATIONS = (0x1 << 7)


class AiLogLevel(enum.IntEnum):
    """Log levels supported by the C inspector library."""
    SUDO = 0x0
    FATAL = 0x1
    ERROR = 0x2
    WARNING = 0x3
    INFO = 0x4
    DEBUG = 0x5
    TRACE = 0x6


class AiBufferFormat(enum.IntEnum):
    """ Map C 'ai_buffer_format' enum to Python enum. """
    AI_BUFFER_FORMAT_NONE = 0x00000040
    AI_BUFFER_FORMAT_FLOAT = 0x01821040
    AI_BUFFER_FORMAT_U8 = 0x00040440
    AI_BUFFER_FORMAT_U16 = 0x00040840
    AI_BUFFER_FORMAT_S8 = 0x00840440
    AI_BUFFER_FORMAT_S16 = 0x00840840
    AI_BUFFER_FORMAT_Q = 0x00840040
    AI_BUFFER_FORMAT_Q7 = 0x00840447
    AI_BUFFER_FORMAT_Q15 = 0x0084084f

    AI_BUFFER_FORMAT_UQ = 0x00040040
    AI_BUFFER_FORMAT_UQ7 = 0x00040447
    AI_BUFFER_FORMAT_UQ15 = 0x0004084f

    """ Buffer Format Flags """
    MASK = (0x01ffffff)       # non-flag bits
    MASK_Q = (0xffffc000)
    TYPE_MASK = (0x019e3f80)  # bits required for type identification
    FBIT_SHIFT = (0x40)       # zero point for signed fractional bits

    FLAG_CONST = (0x1 << 30)
    FLAG_STATIC = (0x1 << 29)
    FLAG_IS_IO = (0x1 << 27)


_AIFMT_TO_NP = {
    AiBufferFormat.AI_BUFFER_FORMAT_NONE: np.void,
    AiBufferFormat.AI_BUFFER_FORMAT_FLOAT: np.float32,
    AiBufferFormat.AI_BUFFER_FORMAT_U8: np.uint8,
    AiBufferFormat.AI_BUFFER_FORMAT_U16: np.uint16,
    AiBufferFormat.AI_BUFFER_FORMAT_S8: np.int8,
    AiBufferFormat.AI_BUFFER_FORMAT_S16: np.int16,
    AiBufferFormat.AI_BUFFER_FORMAT_Q: np.int16,
    AiBufferFormat.AI_BUFFER_FORMAT_Q7: np.int8,
    AiBufferFormat.AI_BUFFER_FORMAT_Q15: np.int16,
    AiBufferFormat.AI_BUFFER_FORMAT_UQ: np.uint16,
    AiBufferFormat.AI_BUFFER_FORMAT_UQ7: np.uint8,
    AiBufferFormat.AI_BUFFER_FORMAT_UQ15: np.uint16
}

_NP_TO_AIFMT = {v: k for k, v in _AIFMT_TO_NP.items()}


def np_to_fmt(dtype, is_io=True, static=False, const=False):
    """Convert 'AiBufferFormat' to numpy dtype."""
    fmt = _NP_TO_AIFMT[dtype].value
    if is_io:
        fmt |= AiBufferFormat.FLAG_IS_IO
    if static:
        fmt |= AiBufferFormat.FLAG_STATIC
    if const:
        fmt |= AiBufferFormat.FLAG_CONST

    return fmt


def fmt_to_np(fmt):
    """Convert 'AiBufferFormat' to numpy dtype."""
    fmt = (fmt & AiBufferFormat.TYPE_MASK) | AiBufferFormat.FBIT_SHIFT
    return _AIFMT_TO_NP[fmt]


class AiIntqInfo(Printable):
    """Wrapper for C 'ai_intq_info' struct."""
    _fields_ = [('scale', ct.POINTER(ct.c_float)),
                ('zeropoint', ct.c_void_p)]


class AiIntqInfoList(Printable):
    """Wrapper for C 'ai_intq_info_list' struct."""
    _fields_ = [('flags', ct.c_uint, 16),
                ('size', ct.c_uint, 16),
                ('info', ct.POINTER(AiIntqInfo))]
    AI_BUFFER_META_FLAG_SCALE_FLOAT = 0x00000001
    AI_BUFFER_META_FLAG_ZEROPOINT_U8 = 0x00000002
    AI_BUFFER_META_FLAG_ZEROPOINT_S8 = 0x00000004


class AiBufferMetaInfo(Printable):
    """Wrapper for C 'ai_buffer_meta_info' struct."""
    _fields_ = [('flags', ct.c_uint, 32),
                ('intq_info_list', ct.POINTER(AiIntqInfoList))]

    """ Meta Info Flags """
    AI_BUFFER_META_HAS_INTQ_INFO = 0x00000001


class AiBuffer(Printable):
    """Wrapper for C 'ai_buffer' struct."""
    _fields_ = [('format', ct.c_uint, 32),
                ('n_batches', ct.c_uint, 16),
                ('height', ct.c_uint, 16),
                ('width', ct.c_uint, 16),
                ('channels', ct.c_uint, 32),
                ('data', ct.c_void_p),
                ('meta_info', ct.POINTER(AiBufferMetaInfo))]

    @classmethod
    def from_ndarray(cls, ndarray, fmt=None):
        """
        Wrap a Numpy array's internal buffer to an AiBuffer without copy.

        Only arrays with 4 dimensions or less can be mapped because AiBuffer
        encodes only 4 dimensions; the Numpy dtype is mapped to the
        corresponding AiBuffer type.
        """
        if len(ndarray.shape) > 4:
            raise ValueError(
                "Arrays with more than 4 dimensions are not supported")

        ndshape = ndarray.shape[:-1] + (1, ) * (4 - len(ndarray.shape)) + \
            (ndarray.shape[-1], )
        if fmt is None:
            fmt = np_to_fmt(ndarray.dtype.type)

        return cls(fmt, *ndshape, data=ct.c_void_p(ndarray.ctypes.data))

    @classmethod
    def from_bytes(cls, bytebuf):
        """Wrap a Python bytearray object to AiBuffer."""
        if not isinstance(bytebuf, bytearray):
            raise RuntimeError("Buffer is not a stream of bytes.")

        data = (ct.POINTER(ct.c_byte * len(bytebuf))).from_buffer(bytebuf)
        return AiBuffer(AiBufferFormat.AI_BUFFER_FORMAT_U8, 1, 1, 1,
                        len(bytebuf), ct.c_void_p(ct.addressof(data)))

    def to_ndarray(self):
        """
        Wrap the AiBuffer internal buffer into a Numpy array object.

        The AiBuffer type is mapped to the corresponding Numpy's dtype.
        The memory is managed by the code which instantiated the AiBuffer;
        create a copy with 'ndarray.copy()' if the buffer was instantiated by
        the C library before unloading the library.
        """
        dtype = fmt_to_np(self.format)
        shape = (self.n_batches, self.height, self.width, self.channels)
        size = np.dtype(dtype).itemsize * int(np.prod(shape))

        # WARNING: the memory is owned by the C library
        # don't let it go out of scope before copying the array content
        data = ct.cast(self.data, ct.POINTER(ct.c_byte * size))
        bytebuf = np.ctypeslib.as_array(data.contents, (size, ))

        return np.reshape(np.frombuffer(bytebuf, dtype), shape)


class AiPlatformVersion(Printable):
    """Wrapper for C 'ai_platform_version' struct."""
    _fields_ = [('major', ct.c_uint, 8),
                ('minor', ct.c_uint, 8),
                ('micro', ct.c_uint, 8),
                ('reserved', ct.c_uint, 8)]


class AiError(Printable):
    """Wrapper for C 'ai_error' struct."""
    _fields_ = [('type', ct.c_uint, 8),
                ('code', ct.c_uint, 24)]


class AiNetworkParams(Printable):
    """Wrapper for C 'ai_network_params' struct."""
    _fields_ = [('params', AiBuffer),
                ('activations', AiBuffer)]


class Network(object):
    """Wrapper for the network library.

    The wrapper accepts an optional parameter 'weight' (a Python bytearray)
    encoding the network's weights. The size and structure of the weights must
    be compatible with the network. The user must either compile the network
    library (*libai_network*) with a *network_data.c* and *network_data.h*
    module or to pass a non-null set of weights, otherwise an error will be
    generated.
    The library is automatically unloaded when the Network object goes out of
    scope.
    """

    def __init__(self, weights=None, lib_path=LIB_PATH):
        self._net = npct.load_library("libai_network", lib_path)
        logging.debug("EmbeddedNet LIB_PATH: %s", lib_path)

        # Wrapper for 'ai_network_create' C API
        self._func_net_create = _wrap_api(
            self._net, 'ai_network_create',
            AiError, [ct.POINTER(ct.c_void_p), ct.POINTER(AiBuffer)])

        # Wrapper for 'ai_network_get_error' C API
        self._func_net_get_error = _wrap_api(
            self._net, 'ai_network_get_error',
            AiError, [ct.c_void_p])

        # Wrapper for 'ai_network_init' C API
        self._func_net_init = _wrap_api(
            self._net, 'ai_network_init',
            ct.c_bool, [ct.c_void_p, ct.POINTER(AiNetworkParams)])

        # Wrapper for 'ai_network_destroy' C API
        self._func_net_destroy = _wrap_api(
            self._net, 'ai_network_destroy', ct.c_void_p, [ct.c_void_p])

        # Wrapper for 'ai_network_get_info' C API
        self._func_net_get_info = _wrap_api(
            self._net, 'ai_network_get_info',
            ct.c_bool, [ct.c_void_p, ct.POINTER(AiNetworkReport)])

        # Wrapper for 'ai_network_run' C API
        self._func_net_run = _wrap_api(
            self._net, 'ai_network_run', ct.c_int,
            [ct.c_void_p, ct.POINTER(AiBuffer), ct.POINTER(AiBuffer)])

        # Wrapper for 'ai_network_forward' C API
        self._func_net_forward = _wrap_api(
            self._net, 'ai_network_forward',
            ct.c_int, [ct.c_void_p, ct.POINTER(AiBuffer)])

        # Wrapper for 'ai_network_data_weights_get' C API
        has_default_weights = True
        try:
            self._func_net_data_weights_get = _wrap_api(
                self._net, 'ai_network_data_weights_get', ct.c_void_p, [])
        except AttributeError:
            has_default_weights = False

        self.handle = ct.c_void_p()
        self.error = self._func_net_create(ct.pointer(self.handle), None)
        logging.debug("net_handle = %s error = %s",
                      str(self.handle), str(self.error))

        # get net info a first time to get activation and parameter sizes
        self.info = AiNetworkReport()
        self._func_net_get_info(self.handle, ct.pointer(self.info))
        self.params = AiNetworkParams(self.info.params, self.info.activations)

        if weights is not None:
            self.params.params = self._load_weights(
                weights, self.params.params.channels)
        elif has_default_weights:
            # Get weights array from C API
            self.params.params.data = ct.cast(
                self._func_net_data_weights_get(), ct.c_void_p)
        elif self.params.params.channels > 0:
            raise RuntimeError("No weights provided and no default weights.")

        # Allocate network activations
        self.params.activations.data = ct.cast(
            (ct.c_ubyte * self.params.activations.channels)(), ct.c_void_p)

        # Initialize the network
        self._func_net_init(self.handle, self.params)

        # get the updated net info
        self._func_net_get_info(self.handle, ct.pointer(self.info))


        self.inputs_in_activations = self._io_is_in_activations(
            self.info.inputs, self.info.n_inputs)
        self.outputs_in_activations = self._io_is_in_activations(
            self.info.outputs, self.info.n_outputs)

        logging.debug("NetEntry.info = %s", str(self.info))

    @staticmethod
    def _load_weights(weights, param_size):
        """Load weights from a byte stream."""
        if param_size != len(weights):
            raise RuntimeError(
                "Weights are incompatible with model: "
                "{} loaded, {} found".format(len(weights), param_size))

        return AiBuffer.from_bytes(weights)

    def _io_is_in_activations(self, io_buffers, io_buffers_size):
        """Check if a list of io_buffers are in the activation space."""
        return any([io_buffers[i].data for i in range(io_buffers_size)])
        # return True if io_buffers[0].data else False

    def get_outputs(self, n_batches):
        """Given the number of batches, it returns the numpy array needed to
        store the outputs."""
        outputs = []
        for idx in range(self.info.n_outputs):
            out_info = self.info.outputs[idx]

            shape = (out_info.height, out_info.width, out_info.channels)
            dtype = fmt_to_np(out_info.format)
            # if output buffer is allocated in activation space
            if self.outputs_in_activations:
                assert n_batches == 1
                out = out_info.to_ndarray()
                # TODO CLEAN
                # print("#### get_outputs in activations: {} {}".format(out.dtype, out))
            else:
                out = np.zeros((n_batches, ) + shape, dtype=dtype)
                # TODO CLEAN
                # print("#### get_outputs: {} {}".format(out.dtype, out))

            outputs.append(np.ascontiguousarray(out))

        return outputs

    @staticmethod
    def _get_buffers(arrays, infos, name):
        """Given a list of numpy arrays and their net info returns a C array of
        buffers."""
        buffers = []
        for idx, arr in enumerate(arrays):
            arr_format = infos[idx].format
            arr_type = np.dtype(fmt_to_np(arr_format))

            if arr_type != arr.dtype:
                raise ValueError(
                    "Wrong {name} format: expected {}, found {} for {name} {}"
                    .format(arr_type, arr.dtype, idx, name=name))

            buffers.append(AiBuffer.from_ndarray(arr, arr_format))

        return (AiBuffer * len(buffers))(*buffers)

    def get_io_buffers(self, inputs, outputs):
        """Given the input and output arrays, it returns the buffers needed to
        run the network and checks if types / shapes are compatible."""
        info = self.info
        assert info.n_inputs == len(inputs) and info.n_outputs == len(outputs)

        return self._get_buffers(inputs, info.inputs, "input"), \
            self._get_buffers(outputs, info.outputs, "output")

    def run(self, inputs):
        """Run the network on the given input.

        Parameters
        ----------
        inputs: ndarray
            network inputs as Numpy array; it must be the same format as the
            network input buffer

        Returns
        -------
        outputs: ndarray
          network execution output as Numpy array
        """
        self._func_net_get_info(self.handle, ct.pointer(self.info))
        logging.debug("NetEntry.run = %s", str(self.info))

        n_batches = inputs[0].shape[0]
        outputs = self.get_outputs(n_batches)

        # TODO CLEAN
        # print("####### Network RUN : batches {}, {}, {}".format(n_batches, inputs, outputs))

        in_buffers, out_buffers = self.get_io_buffers(inputs, outputs)
        run_batches = self._func_net_run(self.handle, in_buffers, out_buffers)

        if run_batches != n_batches:
            raise ValueError("Expected {} batches, executed {}".format(
                n_batches, run_batches))

        return outputs

    def __del__(self):
        if hasattr(self, 'handle') and self.handle:
            self._func_net_destroy(self.handle)


#
# Net inspector
#

class AiNetworkReport(Printable):
    """Wrapper for C 'ai_network_report' struct."""
    _fields_ = [('model_name', ct.c_char_p),
                ('model_signature', ct.c_char_p),
                ('model_datetime', ct.c_char_p),
                ('compile_datetime', ct.c_char_p),
                ('runtime_revision', ct.c_char_p),
                ('runtime_version', AiPlatformVersion),
                ('tool_revision', ct.c_char_p),
                ('tool_version', AiPlatformVersion),
                ('tool_api_version', AiPlatformVersion),
                ('api_version', AiPlatformVersion),
                ('interface_api_version', AiPlatformVersion),
                ('n_macc', ct.c_uint, 32),
                ('n_inputs', ct.c_uint, 16),
                ('n_outputs', ct.c_uint, 16),
                ('inputs', ct.POINTER(AiBuffer)),
                ('outputs', ct.POINTER(AiBuffer)),
                ('activations', AiBuffer),
                ('params', AiBuffer),
                ('n_nodes', ct.c_uint, 32),
                ('signature', ct.c_uint, 32)]


class AiInspectorConfig(Printable):
    """Wrapper for C 'ai_inspector_config' struct."""
    _fields_ = [('validation_mode', ct.c_uint, 8),
                ('log_level', ct.c_uint, 8),
                ('log_quiet', ct.c_bool),
                ('on_report_destroy', ct.c_void_p),
                ('on_exec_node', ct.c_void_p),
                ('cookie', ct.c_void_p)]


class AiInspectorNetEntry(Printable):
    """Wrapper for C 'ai_inspector_net_entry' struct."""
    _fields_ = [('handle', ct.c_void_p),
                ('params', AiNetworkParams),
                ('error', AiError)]


class AiInspectorNodeInfo(Printable):
    """Wrapper for C 'ai_inspector_node_info' struct."""
    _fields_ = [('type', ct.c_uint, 16),
                ('id', ct.c_uint, 16),
                ('batch_id', ct.c_uint, 16),
                ('n_batches', ct.c_uint, 16),
                ('elapsed_ms', ct.c_float),
                ('in_size', ct.c_uint, 16),
                ('out_size', ct.c_uint, 16),
                ('in', ct.POINTER(AiBuffer)),
                ('out', ct.POINTER(AiBuffer))]


class AiInspectorNetReport(Printable):
    """Wrapper for C 'ai_inspector_net_report' struct."""
    _fields_ = [('id', ct.c_uint, 32),
                ('signature', ct.c_uint, 32),
                ('num_inferences', ct.c_uint, 32),
                ('n_nodes', ct.c_uint, 32),
                ('elapsed_ms', ct.c_float),
                ('node', ct.POINTER(AiInspectorNodeInfo))]


class NetInspector(object):
    """Wrapper for the inspector library.

    Parameters
    ----------
    weights: bytearray
      weights of the wrapped library as a byte buffer
    mode: AiInspectMode
      type of inspection; may be VALIDATION, PROFILING and DYNAMIC_RANGE
      analysis
    log_level: AiLogLevel
      verbosity level of logs generated by the C library code
    log_quiet: Boolean
      completely suppress log messages in the C library
    """
    _DEFAULT_MODE = AiInspectMode.VALIDATION_INSPECT | \
        AiInspectMode.STORE_ALL_IO_ACTIVATIONS

    def __init__(self, weights=None, log_level=AiLogLevel.ERROR,
                 log_quiet=True, lib_path=LIB_PATH):

        self._inspector = npct.load_library("libai_inspector", lib_path)

        # Wrapper for 'ai_inspector_default_config' C API
        self._func_get_default_config = _wrap_api(
            self._inspector, 'ai_inspector_default_config', AiInspectorConfig,
            [])

        # Wrapper for 'ai_inspector_create' C API
        self._func_inspector_create = _wrap_api(
            self._inspector, 'ai_inspector_create', ct.c_bool,
            [ct.POINTER(ct.c_void_p), ct.POINTER(AiInspectorConfig)])

        # Wrapper for 'ai_inspector_destroy' C API
        self._func_inspector_destroy = _wrap_api(
            self._inspector, 'ai_inspector_destroy', ct.c_bool,
            [ct.c_void_p])

        # Wrapper for 'ai_inspector_bind_network' C API
        self._func_inspector_bind_net = _wrap_api(
            self._inspector, 'ai_inspector_bind_network', ct.c_uint,
            [ct.c_void_p, ct.POINTER(AiInspectorNetEntry)])

        # Wrapper for 'ai_inspector_unbind_network' C API
        self._func_inspector_unbind_net = _wrap_api(
            self._inspector, 'ai_inspector_unbind_network', ct.c_bool,
            [ct.c_void_p, ct.c_uint])

        # Wrapper for 'ai_inspector_get_report' C API
        self._func_inspector_report_get = _wrap_api(
            self._inspector, 'ai_inspector_get_report', ct.c_bool,
            [ct.c_void_p, ct.c_uint, ct.POINTER(AiInspectorNetReport)])

        # Wrapper for 'ai_inspector_run' C API
        self._func_inspector_run = _wrap_api(
            self._inspector, 'ai_inspector_run', ct.c_int,
            [ct.c_void_p, ct.c_uint,
             ct.POINTER(AiBuffer), ct.POINTER(AiBuffer)])

        config = AiInspectorConfig(
            self._DEFAULT_MODE, log_level, log_quiet, None, None, None)
        self._handle = ct.c_void_p()

        self._func_inspector_create(ct.pointer(self._handle),
                                    ct.pointer(config))

        logging.debug("self._handle = %s", str(self._handle))

        self.net = Network(weights, lib_path=lib_path)
        net_entry = AiInspectorNetEntry(
            self.net.handle, self.net.params, self.net.error)
        logging.debug("NetInspector.net_entry = %s", str(net_entry))

        self._net_id = self._func_inspector_bind_net(
            self._handle, ct.pointer(net_entry))
        logging.debug("net init: %s", str(self.net.handle))

    def run(self, inputs):
        """Run the wrapped network on the given input and generate the
        inspection report.

        Parameters
        ----------
        inputs: ndarray
          network inputs as Numpy array; it must be the same format as the
          network input buffer

        Returns
        -------
        outputs: ndarray
          network execution output as Numpy array
        """
        logging.debug("net run: %s %s", str(self.net.handle),
                      str(self.net.params.params.data))

        n_batches = inputs[0].shape[0]
        outputs = self.net.get_outputs(n_batches)
        in_buffers, out_buffers = self.net.get_io_buffers(inputs, outputs)

        run_batches = self._func_inspector_run(
            self._handle, self._net_id, in_buffers, out_buffers)

        if run_batches != n_batches:
            raise ValueError("Expected {} batches, executed {}".format(
                n_batches, run_batches))

        return outputs

    def get_run_report(self):
        """Return the report of the network execution."""
        report = AiInspectorNetReport()
        self._func_inspector_report_get(self._handle, self._net_id,
                                        ct.pointer(report))
        return report

    def __del__(self):
        # WARNING: it releases the context but it doesn't unload the library
        if hasattr(self, '_net_id') and self._net_id:
            self._func_inspector_unbind_net(self._handle, self._net_id)
        if hasattr(self, '_inspector') and self._inspector:
            self._func_inspector_destroy(self._handle)


def main():
    """Script entry point."""
    import argparse
    import sys

    parser = argparse.ArgumentParser(
        description="Example of usage of the inspector class")
    parser.add_argument('--input', '-i', type=str,
                        help="input data as CSV or NPY file")
    parser.add_argument('--standalone', action='store_true',
                        help='Runs only the network library without inspector')
    args = parser.parse_args()

    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    np.random.seed(42)

    if args.standalone:
        # Test standalone network library
        model = Network()
        in_info = model.info.inputs[0]
    else:
        # Test inspector library
        model = NetInspector()
        in_info = model.net.info.inputs[0]

    in_shape = (in_info.height, in_info.width, in_info.channels)

    if args.input:
        in_values = np.atleast_2d(np.genfromtxt(args.input, delimiter=','))
        assert np.prod(in_shape) == in_values.shape[1]
    else:
        n_batches = 1
        in_values = np.random.randn(n_batches, *in_shape)

    in_values = np.ascontiguousarray(in_values.astype(np.float32))

    # run twice to check memory handling
    for _ in range(2):
        out_values = model.run(in_values)
        print("\nRun results:")
        for out in out_values:
            print(out[0, 0, :])
        print("")

        if not args.standalone:
            report = model.get_run_report()
            print("Report: {}".format(report))
            # test access to intermediate features
            for i in range(report.n_nodes):
                report.node[i].out[0].to_ndarray()


if __name__ == '__main__':
    main()
