/**
  ******************************************************************************
  * @file    network.c
  * @author  AST Embedded Analytics Research Platform
  * @date    Tue Feb 19 15:27:18 2019
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */



#include "network.h"

#include "ai_platform_interface.h"
#include "ai_math_helpers.h"

#include "core_common.h"
#include "layers.h"

#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#define AI_TOOLS_VERSION_MAJOR 3
#define AI_TOOLS_VERSION_MINOR 3
#define AI_TOOLS_VERSION_MICRO 0

#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#define AI_TOOLS_API_VERSION_MAJOR 1
#define AI_TOOLS_API_VERSION_MINOR 1
#define AI_TOOLS_API_VERSION_MICRO 0

#undef AI_NET_OBJ_INSTANCE
#define AI_NET_OBJ_INSTANCE g_network
 
#undef AI_NETWORK_MODEL_SIGNATURE
#define AI_NETWORK_MODEL_SIGNATURE     "f91b9cfbf23a0ec03538016d3a21c147"

#ifndef AI_TOOLS_REVISION_ID
#define AI_TOOLS_REVISION_ID     "(rev-c293280b493e66f6c7a8e61e742c8b740419fe74)"
#endif

#undef AI_TOOLS_DATE_TIME
#define AI_TOOLS_DATE_TIME   "Tue Feb 19 15:27:18 2019"

#undef AI_TOOLS_COMPILE_TIME
#define AI_TOOLS_COMPILE_TIME    __DATE__ " " __TIME__

#undef AI_NETWORK_N_BATCHES
#define AI_NETWORK_N_BATCHES         (1)

#define SHAPE_INIT(a_, b_, c_, d_) \
  AI_SHAPE_INIT(4, (d_), (c_), (b_), (a_))

#define SHAPE_2D_INIT(h_, w_) \
  AI_SHAPE_2D_INIT((w_), (h_))

#define STRIDE_INIT(a_, b_, c_, d_) \
  AI_STRIDE_INIT(4, (d_), (c_), (b_), (a_))

#define TENSORS(...) \
  __VA_ARGS__

/**  Forward network declaration section  *************************************/
AI_STATIC ai_network AI_NET_OBJ_INSTANCE;


/**  Forward network array declarations  **************************************/
AI_STATIC ai_array conv2d_7_scratch0_array;   /* Array #0 */
AI_STATIC ai_array conv2d_4_scratch0_array;   /* Array #1 */
AI_STATIC ai_array conv2d_1_scratch0_array;   /* Array #2 */
AI_STATIC ai_array dense_10_bias_array;   /* Array #3 */
AI_STATIC ai_array dense_10_weights_array;   /* Array #4 */
AI_STATIC ai_array conv2d_7_bias_array;   /* Array #5 */
AI_STATIC ai_array conv2d_7_weights_array;   /* Array #6 */
AI_STATIC ai_array conv2d_4_bias_array;   /* Array #7 */
AI_STATIC ai_array conv2d_4_weights_array;   /* Array #8 */
AI_STATIC ai_array conv2d_1_bias_array;   /* Array #9 */
AI_STATIC ai_array conv2d_1_weights_array;   /* Array #10 */
AI_STATIC ai_array input_0_output_array;   /* Array #11 */
AI_STATIC ai_array conv2d_1_output_array;   /* Array #12 */
AI_STATIC ai_array conv2d_4_output_array;   /* Array #13 */
AI_STATIC ai_array conv2d_7_output_array;   /* Array #14 */
AI_STATIC ai_array dense_10_output_array;   /* Array #16 */
AI_STATIC ai_array nonlinearity_11_output_array;   /* Array #17 */


/**  Forward network tensor declarations  *************************************/
AI_STATIC ai_tensor conv2d_7_scratch0;   /* Tensor #0 */
AI_STATIC ai_tensor conv2d_4_scratch0;   /* Tensor #1 */
AI_STATIC ai_tensor conv2d_1_scratch0;   /* Tensor #2 */
AI_STATIC ai_tensor dense_10_bias;   /* Tensor #3 */
AI_STATIC ai_tensor dense_10_weights;   /* Tensor #4 */
AI_STATIC ai_tensor conv2d_7_bias;   /* Tensor #5 */
AI_STATIC ai_tensor conv2d_7_weights;   /* Tensor #6 */
AI_STATIC ai_tensor conv2d_4_bias;   /* Tensor #7 */
AI_STATIC ai_tensor conv2d_4_weights;   /* Tensor #8 */
AI_STATIC ai_tensor conv2d_1_bias;   /* Tensor #9 */
AI_STATIC ai_tensor conv2d_1_weights;   /* Tensor #10 */
AI_STATIC ai_tensor input_0_output;   /* Tensor #11 */
AI_STATIC ai_tensor conv2d_1_output;   /* Tensor #12 */
AI_STATIC ai_tensor conv2d_4_output;   /* Tensor #13 */
AI_STATIC ai_tensor conv2d_7_output;   /* Tensor #14 */
AI_STATIC ai_tensor conv2d_7_output0;   /* Tensor #15 */
AI_STATIC ai_tensor dense_10_output;   /* Tensor #16 */
AI_STATIC ai_tensor nonlinearity_11_output;   /* Tensor #17 */


/**  Forward network tensor chain declarations  *******************************/
AI_STATIC_CONST ai_tensor_chain conv2d_1_chain;   /* Chain #0 */
AI_STATIC_CONST ai_tensor_chain conv2d_4_chain;   /* Chain #1 */
AI_STATIC_CONST ai_tensor_chain conv2d_7_chain;   /* Chain #2 */
AI_STATIC_CONST ai_tensor_chain dense_10_chain;   /* Chain #3 */
AI_STATIC_CONST ai_tensor_chain nonlinearity_11_chain;   /* Chain #4 */


/**  Subgraph network operator tensor chain declarations  *********************/


/**  Subgraph network operator declarations  *********************************/


/**  Forward network layer declarations  **************************************/
AI_STATIC ai_layer_conv2d_nl_pool conv2d_1_layer; /* Layer #0 */
AI_STATIC ai_layer_conv2d_nl_pool conv2d_4_layer; /* Layer #1 */
AI_STATIC ai_layer_conv2d_nl_pool conv2d_7_layer; /* Layer #2 */
AI_STATIC ai_layer_dense dense_10_layer; /* Layer #3 */
AI_STATIC ai_layer_nl nonlinearity_11_layer; /* Layer #4 */


/**  Array declarations section  **********************************************/
AI_ARRAY_OBJ_DECLARE(
  conv2d_7_scratch0_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 320,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_4_scratch0_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 640,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_1_scratch0_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 1024,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  dense_10_bias_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 10,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  dense_10_weights_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 3200,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_7_bias_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 20,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_7_weights_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 10000,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_4_bias_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 20,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_4_weights_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 8000,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_1_bias_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 16,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_1_weights_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 1200,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  input_0_output_array, AI_ARRAY_FORMAT_FLOAT|AI_FMT_FLAG_IS_IO,
  NULL, NULL, 3072,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_1_output_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 4096,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_4_output_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 1280,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  conv2d_7_output_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 320,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  dense_10_output_array, AI_ARRAY_FORMAT_FLOAT,
  NULL, NULL, 10,
  AI_STATIC)
AI_ARRAY_OBJ_DECLARE(
  nonlinearity_11_output_array, AI_ARRAY_FORMAT_FLOAT|AI_FMT_FLAG_IS_IO,
  NULL, NULL, 10,
  AI_STATIC)


/**  Tensor declarations section  *********************************************/
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_scratch0, AI_STATIC, 
  0x0, 4, SHAPE_INIT(2, 8, 20, 1),
  STRIDE_INIT(640, 80, 4, 4),
  1, &conv2d_7_scratch0_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_scratch0, AI_STATIC, 
  0x0, 4, SHAPE_INIT(2, 16, 20, 1),
  STRIDE_INIT(1280, 80, 4, 4),
  1, &conv2d_4_scratch0_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_1_scratch0, AI_STATIC, 
  0x0, 4, SHAPE_INIT(2, 32, 16, 1),
  STRIDE_INIT(2048, 64, 4, 4),
  1, &conv2d_1_scratch0_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  dense_10_bias, AI_STATIC, 
  0x0, 4, SHAPE_INIT(1, 1, 10, 1),
  STRIDE_INIT(40, 40, 4, 4),
  1, &dense_10_bias_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  dense_10_weights, AI_STATIC, 
  0x0, 4, SHAPE_INIT(1, 1, 10, 320),
  STRIDE_INIT(12800, 12800, 1280, 4),
  1, &dense_10_weights_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_bias, AI_STATIC, 
  0x0, 4, SHAPE_INIT(1, 1, 20, 1),
  STRIDE_INIT(80, 80, 4, 4),
  1, &conv2d_7_bias_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_weights, AI_STATIC, 
  0x0, 4, SHAPE_INIT(20, 5, 5, 20),
  STRIDE_INIT(2000, 400, 80, 4),
  1, &conv2d_7_weights_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_bias, AI_STATIC, 
  0x0, 4, SHAPE_INIT(1, 1, 20, 1),
  STRIDE_INIT(80, 80, 4, 4),
  1, &conv2d_4_bias_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_weights, AI_STATIC, 
  0x0, 4, SHAPE_INIT(20, 5, 5, 16),
  STRIDE_INIT(1600, 320, 64, 4),
  1, &conv2d_4_weights_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_1_bias, AI_STATIC, 
  0x0, 4, SHAPE_INIT(1, 1, 16, 1),
  STRIDE_INIT(64, 64, 4, 4),
  1, &conv2d_1_bias_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_1_weights, AI_STATIC, 
  0x0, 4, SHAPE_INIT(16, 5, 5, 3),
  STRIDE_INIT(300, 60, 12, 4),
  1, &conv2d_1_weights_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  input_0_output, AI_STATIC, 
  0x0, 4, SHAPE_INIT(32, 32, 3, 1),
  STRIDE_INIT(384, 12, 4, 4),
  1, &input_0_output_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_1_output, AI_STATIC, 
  0x0, 4, SHAPE_INIT(16, 16, 16, 1),
  STRIDE_INIT(1024, 64, 4, 4),
  1, &conv2d_1_output_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_4_output, AI_STATIC, 
  0x0, 4, SHAPE_INIT(8, 8, 20, 1),
  STRIDE_INIT(640, 80, 4, 4),
  1, &conv2d_4_output_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_output, AI_STATIC, 
  0x0, 4, SHAPE_INIT(4, 4, 20, 1),
  STRIDE_INIT(320, 80, 4, 4),
  1, &conv2d_7_output_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  conv2d_7_output0, AI_STATIC, 
  0x0, 4, SHAPE_INIT(1, 1, 320, 1),
  STRIDE_INIT(1280, 1280, 4, 4),
  1, &conv2d_7_output_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  dense_10_output, AI_STATIC, 
  0x0, 4, SHAPE_INIT(1, 1, 10, 1),
  STRIDE_INIT(40, 40, 4, 4),
  1, &dense_10_output_array,
  NULL)
AI_TENSOR_OBJ_DECLARE(
  nonlinearity_11_output, AI_STATIC, 
  0x0, 4, SHAPE_INIT(1, 1, 10, 1),
  STRIDE_INIT(40, 40, 4, 4),
  1, &nonlinearity_11_output_array,
  NULL)


/**  Layer declarations section  **********************************************/



AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_1_chain, AI_STATIC_CONST, 4,
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&input_0_output)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_1_output)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 2, TENSORS(&conv2d_1_weights, &conv2d_1_bias)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_1_scratch0))
)

AI_LAYER_OBJ_DECLARE(
  conv2d_1_layer, 1,
  OPTIMIZED_CONV2D_TYPE,
  conv2d_nl_pool, forward_conv2d_nl_pool,
  &AI_NET_OBJ_INSTANCE, &conv2d_4_layer, AI_STATIC,
  .tensors = &conv2d_1_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = SHAPE_2D_INIT(1, 1), 
  .dilation = SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 2, 2, 2, 2), 
  .pool_size = SHAPE_2D_INIT(2, 2), 
  .pool_stride = SHAPE_2D_INIT(2, 2), 
  .pool_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
  .pool_func = pool_func_mp_array_f32, 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_4_chain, AI_STATIC_CONST, 4, 
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_1_output)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_4_output)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 2, TENSORS(&conv2d_4_weights, &conv2d_4_bias)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_4_scratch0))
)

AI_LAYER_OBJ_DECLARE(
  conv2d_4_layer, 4,
  OPTIMIZED_CONV2D_TYPE,
  conv2d_nl_pool, forward_conv2d_nl_pool,
  &AI_NET_OBJ_INSTANCE, &conv2d_7_layer, AI_STATIC,
  .tensors = &conv2d_4_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = SHAPE_2D_INIT(1, 1), 
  .dilation = SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 2, 2, 2, 2), 
  .pool_size = SHAPE_2D_INIT(2, 2), 
  .pool_stride = SHAPE_2D_INIT(2, 2), 
  .pool_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
  .pool_func = pool_func_mp_array_f32, 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  conv2d_7_chain, AI_STATIC_CONST, 4, 
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_4_output)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_7_output)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 2, TENSORS(&conv2d_7_weights, &conv2d_7_bias)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_7_scratch0))
)

AI_LAYER_OBJ_DECLARE(
  conv2d_7_layer, 7,
  OPTIMIZED_CONV2D_TYPE,
  conv2d_nl_pool, forward_conv2d_nl_pool,
  &AI_NET_OBJ_INSTANCE, &dense_10_layer, AI_STATIC,
  .tensors = &conv2d_7_chain, 
  .groups = 1, 
  .nl_func = nl_func_relu_array_f32, 
  .filter_stride = SHAPE_2D_INIT(1, 1), 
  .dilation = SHAPE_2D_INIT(1, 1), 
  .filter_pad = AI_SHAPE_INIT(4, 2, 2, 2, 2), 
  .pool_size = SHAPE_2D_INIT(2, 2), 
  .pool_stride = SHAPE_2D_INIT(2, 2), 
  .pool_pad = AI_SHAPE_INIT(4, 0, 0, 0, 0), 
  .pool_func = pool_func_mp_array_f32, 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  dense_10_chain, AI_STATIC_CONST, 4, 
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&conv2d_7_output0)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&dense_10_output)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 2, TENSORS(&dense_10_weights, &dense_10_bias)),
  AI_TENSOR_LIST_OBJ_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  dense_10_layer, 10,
  DENSE_TYPE,
  dense, forward_dense,
  &AI_NET_OBJ_INSTANCE, &nonlinearity_11_layer, AI_STATIC,
  .tensors = &dense_10_chain, 
)

AI_TENSOR_CHAIN_OBJ_DECLARE(
  nonlinearity_11_chain, AI_STATIC_CONST, 4, 
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&dense_10_output)),
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, 1, TENSORS(&nonlinearity_11_output)),
  AI_TENSOR_LIST_OBJ_EMPTY,
  AI_TENSOR_LIST_OBJ_EMPTY
)

AI_LAYER_OBJ_DECLARE(
  nonlinearity_11_layer, 11,
  NL_TYPE,
  nl, forward_sm,
  &AI_NET_OBJ_INSTANCE, &nonlinearity_11_layer, AI_STATIC,
  .tensors = &nonlinearity_11_chain, 
)


AI_NETWORK_OBJ_DECLARE(
  AI_NET_OBJ_INSTANCE, AI_STATIC, 
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, 89864, 1,
                     NULL),
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, 24068, 1,
                     NULL),
  AI_TENSOR_LIST_IO_OBJ_INIT(AI_FLAG_NONE, AI_NETWORK_IN_NUM, &input_0_output),
  AI_TENSOR_LIST_IO_OBJ_INIT(AI_FLAG_NONE, AI_NETWORK_OUT_NUM, &nonlinearity_11_output),
  &conv2d_1_layer, 0, NULL)


AI_DECLARE_STATIC
ai_bool network_configure_activations(
  ai_network* net_ctx, const ai_buffer* activation_buffer)
{
  AI_ASSERT(net_ctx &&  activation_buffer && activation_buffer->data)

  ai_ptr activations = AI_PTR(AI_PTR_ALIGN(activation_buffer->data, 4));
  AI_ASSERT( activations )

  {
    /* Updating activations (byte) offsets */
    conv2d_7_scratch0_array.data = activations + 0;
    conv2d_7_scratch0_array.data_start = activations + 0;
    conv2d_4_scratch0_array.data = activations + 0;
    conv2d_4_scratch0_array.data_start = activations + 0;
    conv2d_1_scratch0_array.data = activations + 0;
    conv2d_1_scratch0_array.data_start = activations + 0;
    input_0_output_array.data = NULL;
    input_0_output_array.data_start = NULL;
    conv2d_1_output_array.data = activations + 7680;
    conv2d_1_output_array.data_start = activations + 7680;
    conv2d_4_output_array.data = activations + 2560;
    conv2d_4_output_array.data_start = activations + 2560;
    conv2d_7_output_array.data = activations + 1280;
    conv2d_7_output_array.data_start = activations + 1280;
    dense_10_output_array.data = activations + 0;
    dense_10_output_array.data_start = activations + 0;
    nonlinearity_11_output_array.data = NULL;
    nonlinearity_11_output_array.data_start = NULL;
    
  }
  return true;
}


AI_DECLARE_STATIC
ai_bool network_configure_weights(
  ai_network* net_ctx, const ai_buffer* weights_buffer)
{
  AI_ASSERT(net_ctx &&  weights_buffer && weights_buffer->data)

  ai_ptr weights = AI_PTR(weights_buffer->data);
  AI_ASSERT( weights )

  {
    /* Updating weights (byte) offsets */
    dense_10_bias_array.format |= AI_FMT_FLAG_CONST;
    dense_10_bias_array.data = weights + 89824;
    dense_10_bias_array.data_start = weights + 89824;
  dense_10_weights_array.format |= AI_FMT_FLAG_CONST;
    dense_10_weights_array.data = weights + 77024;
    dense_10_weights_array.data_start = weights + 77024;
  conv2d_7_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_7_bias_array.data = weights + 76944;
    conv2d_7_bias_array.data_start = weights + 76944;
  conv2d_7_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_7_weights_array.data = weights + 36944;
    conv2d_7_weights_array.data_start = weights + 36944;
  conv2d_4_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_4_bias_array.data = weights + 36864;
    conv2d_4_bias_array.data_start = weights + 36864;
  conv2d_4_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_4_weights_array.data = weights + 4864;
    conv2d_4_weights_array.data_start = weights + 4864;
  conv2d_1_bias_array.format |= AI_FMT_FLAG_CONST;
    conv2d_1_bias_array.data = weights + 4800;
    conv2d_1_bias_array.data_start = weights + 4800;
  conv2d_1_weights_array.format |= AI_FMT_FLAG_CONST;
    conv2d_1_weights_array.data = weights + 0;
    conv2d_1_weights_array.data_start = weights + 0;
  
  }

  return true;
}


/**  PUBLIC APIs SECTION  *****************************************************/

AI_API_ENTRY
ai_bool ai_network_get_info(
  ai_handle network, ai_network_report* report)
{
  ai_network* net_ctx = AI_NETWORK_ACQUIRE_CTX(network);

  if ( report && net_ctx )
  {
    ai_network_report r = {
      .model_name        = AI_NETWORK_MODEL_NAME,
      .model_signature   = AI_NETWORK_MODEL_SIGNATURE,
      .model_datetime    = AI_TOOLS_DATE_TIME,
      
      .compile_datetime  = AI_TOOLS_COMPILE_TIME,
      
      .runtime_revision  = ai_platform_runtime_get_revision(),
      .runtime_version   = ai_platform_runtime_get_version(),

      .tool_revision     = AI_TOOLS_REVISION_ID,
      .tool_version      = {AI_TOOLS_VERSION_MAJOR, AI_TOOLS_VERSION_MINOR,
                            AI_TOOLS_VERSION_MICRO, 0x0},
      .tool_api_version  = {AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR,
                            AI_TOOLS_API_VERSION_MICRO, 0x0},

      .api_version            = ai_platform_api_get_version(),
      .interface_api_version  = ai_platform_interface_api_get_version(),
      
      .n_macc            = 3965774,
      .n_inputs          = 0,
      .inputs            = NULL,
      .n_outputs         = 0,
      .outputs           = NULL,
      .activations       = AI_STRUCT_INIT,
      .params            = AI_STRUCT_INIT,
      .n_nodes           = 0,
      .signature         = 0x0,
    };

    if ( !ai_platform_api_get_network_report(network, &r) ) return false;

    *report = r;
    return true;
  }

  return false;
}

AI_API_ENTRY
ai_error ai_network_get_error(ai_handle network)
{
  return ai_platform_network_get_error(network);
}

AI_API_ENTRY
ai_error ai_network_create(
  ai_handle* network, const ai_buffer* network_config)
{
  return ai_platform_network_create(
    network, network_config, 
    &AI_NET_OBJ_INSTANCE,
    AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR, AI_TOOLS_API_VERSION_MICRO);
}

AI_API_ENTRY
ai_handle ai_network_destroy(ai_handle network)
{
  return ai_platform_network_destroy(network);
}

AI_API_ENTRY
ai_bool ai_network_init(
  ai_handle network, const ai_network_params* params)
{
  ai_network* net_ctx = ai_platform_network_init(network, params);
  if ( !net_ctx ) return false;

  ai_bool ok = true;
  ok &= network_configure_weights(net_ctx, &params->params);
  ok &= network_configure_activations(net_ctx, &params->activations);

  return ok;
}


AI_API_ENTRY
ai_i32 ai_network_run(
  ai_handle network, const ai_buffer* input, ai_buffer* output)
{
  return ai_platform_network_process(network, input, output);
}

AI_API_ENTRY
ai_i32 ai_network_forward(ai_handle network, const ai_buffer* input)
{
  return ai_platform_network_process(network, input, NULL);
}

#undef TENSORS
#undef SHAPE_INIT
#undef SHAPE_2D_INIT
#undef STRIDE_INIT

#undef AI_NETWORK_MODEL_SIGNATURE
#undef AI_NET_OBJ_INSTANCE
#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#undef AI_TOOLS_DATE_TIME
#undef AI_TOOLS_COMPILE_TIME

