/* jslint node: true */
/* jshint esversion: 6 */
"use strict";
var fs = require('fs');
var convnetjs = require('convnetjs');
var program = require('commander');


/** convert parsed CSV rows to float arrays */
function parse_csv(rows) {
    var images = [];
    for (var i = 0; i < (rows.length - 1); ++i) {
        var row = rows[i].toString().split(',');

        for (var j = 0; j < row.length; ++j) {
            row[j] = parseFloat(row[j]);
        }
        images.push(row);
    }
    return images;
}


/** run network on the given images and compute output probabilities */
function run_network(model_file, images) {
    // load network from JSON dump
    var net = new convnetjs.Net();
    // read file and strip UTF8 BOM markers
    var json_str = fs.readFileSync(model_file, 'utf8').replace(/^\uFEFF/, '');
    net.fromJSON(JSON.parse(json_str));
    
    var in_layer = net.layers[0];
    var outputs = [];

    for (var i = 0; i < images.length; ++i) {
        var input = new convnetjs.Vol(
            in_layer.out_sy, in_layer.out_sy, in_layer.out_depth, 0.0);
        input.w = images[i];
        // use act = net.layer[j].forward(act); to access internal activations
        outputs.push(Array.prototype.slice.call(net.forward(input).w));
    }

    return outputs;
}


/** program entry point */
function main(model_file, input_file, output_file=null) {
    // load data from CSV (one image per line)
    var rows = fs.readFileSync(input_file).toString().split('\n');
    var images = parse_csv(rows);

    var outputs = run_network(model_file, images);
    if (output_file) {
        fs.writeFileSync(output_file, JSON.stringify([outputs]));
    } else {
        for (var i = 0; i < outputs.length; ++i) {
            console.log("%s ", Array.from(outputs[i]).join(', '));
        }
    }
}

exports.run_network = run_network;


/** Usage: node test_convnet.js <model.json> <input.csv> */
if (require.main === module) {
    // execute only if called as a script
    program
        .arguments('<model> <input> [output]')
        .action(function(model, input, output) {
            main(model, input, output);
        })
        .parse(process.argv);
}
