{%- import 'network_layers.j2.c' as layers -%}
{% set net_name = config['net_name'].lower() -%}

/**
  ******************************************************************************
  * @file    op_binary_func_custom.c
  * @author  AST Embedded Analytics Research Platform
  * @date    {{ config['date_time'] }}
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#include "op_binary_func_custom.h"
#include "ai_math_helpers.h"

{% for graph in config[layers.FUNCTIONS]: %}
{{layers.custom_func_body(graph)}}
{% endfor -%}
