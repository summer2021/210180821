{% set LAYERS = 'layers' -%}
{% set TENSORS = 'tensors' -%}
{% set ARRAYS = 'arrays' -%}
{% set FUNCTIONS = 'functions' -%}
{% set FORMATS = 'formats' -%}
{% set TAGS = 'tags' -%}


{# Arrays Macro Section #}

{%- macro array_forward_declaration(conf, is_const=False) -%}
{% if is_const %}AI_STATIC_CONST{% else %}AI_STATIC{% endif %} ai_array {{ conf['name'] }};
{%- endmacro -%}

{%- macro declare_param_arrays(conf) -%}
AI_STATIC_CONST {{conf['c_type']}} {{conf['name']}}_data[] = { {{conf['init']}} };
AI_ARRAY_OBJ_DECLARE(
    {{conf['name']}}, {{conf['fmt_data']}},
    {{conf['name']}}_data, {{conf['name']}}_data, {{conf['size']}}, AI_STATIC_CONST)
{%- endmacro -%}

{%- macro declare_array(conf, index, is_const=False) -%}
/* Array#{{ index }} */
AI_ARRAY_OBJ_DECLARE(
  {{conf['name']}}, {{conf['fmt_data']}},
  {{conf['p_data']}}, {{conf['p_data']}}, {{conf['size']}}, AI_STATIC{%- if is_const -%}_CONST{%- endif -%})
{% endmacro %}

{# Tensors Macro Section #}

{%- macro tensor_forward_declaration(conf, is_const=False) -%}
AI_STATIC{%- if is_const -%}_CONST{% endif %} ai_tensor {{ conf['name'] }};
{%- endmacro -%}

{%- macro declare_tensor(conf, index, is_const=False) -%}
/* Tensor #{{ index }} */
AI_TENSOR_OBJ_DECLARE(
  {{ conf['name'] }}, AI_STATIC{%- if is_const -%}_CONST{%- endif -%},
  0x0, 0x0,
  AI_SHAPE_INIT({{ conf['dimensions'] }}), AI_STRIDE_INIT({{ conf['strides'] }}),
  1, {{ conf['array'] }}, {{ conf['intq'] }})
{% endmacro %}

{%- macro declare_intq(conf, index, is_const=False) -%}
/* Int quant #{{ index }} */
AI_INTQ_INFO_LIST_OBJ_DECLARE({{ conf['name'] }}, AI_STATIC{%- if is_const -%}_CONST{%- endif -%},
  {{ conf['flags'] }}, {{ conf['size'] }},
  AI_PACK_INTQ_INFO(
    {{ conf['scale'] }},
    {{ conf['zero'] }}))
{% endmacro %}

{# Layers Tensorchains Macro Section #}

{%- macro declare_tensor_list(list) -%}
  {%- if list|length>0 -%}
  AI_TENSOR_LIST_OBJ_INIT(AI_FLAG_NONE, {{ list|length }}, {{ list|join(', ') }})
  {%- else -%}
  AI_TENSOR_LIST_OBJ_EMPTY
  {%- endif -%}
{%- endmacro -%}

{%- macro tensor_chain_forward_declare(chain, is_const=True) -%}
{% if is_const %}AI_STATIC_CONST{% else%}AI_STATIC{% endif %} ai_tensor_chain {{ chain['name'] }};
{%- endmacro -%}

{%- macro declare_tensor_chain(chain, is_const=True) -%}
AI_TENSOR_CHAIN_OBJ_DECLARE(
  {{ chain['name'] }}, {% if is_const %}AI_STATIC_CONST{% else%}AI_STATIC{% endif %}, 4,{{"\n  "-}}
  {{ declare_tensor_list(chain['input']) }},
  {{ declare_tensor_list(chain['output']) }},
  {{ declare_tensor_list(chain['weights']) }},
  {{ declare_tensor_list(chain['scratch']) }}
)
{%- endmacro -%}

{# Layers Macro Section #}

{%- macro layer_forward_declaration(layer) -%}
{% if is_const %}AI_STATIC_CONST{% else%}AI_STATIC{% endif %} ai_layer_{{ layer[TAGS]['c_layer_postfix'] }} {{ layer['layer_name'] }};
{%- endmacro -%}

{%- macro declare_layer_object(layer, net_obj) -%}
AI_LAYER_OBJ_DECLARE(
  {{ layer['layer_name'] }}, {{ layer[TAGS]['c_layer_id'] }},
  {{ layer[TAGS]['c_layer_type'].upper() }},
  {{ layer[TAGS]['c_layer_postfix'] }}, {{ layer[TAGS]['c_layer_forward'] }},
  &{{ net_obj }}, &{{ layer['next'] }}, {% if is_const %}AI_STATIC_CONST{% else %}AI_STATIC{% endif %},
  .tensors = &{{ layer['tensor_chain']['name'] }},{{" "-}}
  {% for param_str in layer[TAGS]['c_param_strings']: %}
  {{ param_str }},{{" "-}}
  {%- endfor %}
)
{%- endmacro -%}

{%- macro declare_layer(layer, net_obj) -%}
{%   for array in layer["arrays"]: %}
{{     declare_param_arrays(array) }}
{%   endfor -%}

{{ declare_tensor_chain(layer['tensor_chain']) }}

{{ declare_layer_object(layer, net_obj) }}

{%- endmacro -%}


{# Offsets Computation Macro Section #}

{%- macro weights_init_offsets(arrays) -%}
{%- for array in arrays: -%}
  {%- if array['is_const'] %}
    {{ array['name'] }}.format |= AI_FMT_FLAG_CONST;
    {{ array['name'] }}.data = AI_PTR(weights + {{ array['offset'] }});
    {{ array['name'] }}.data_start = AI_PTR(weights + {{ array['start_offset'] }});
  {%- endif -%}
{%- endfor -%}
{%- endmacro -%}

{%- macro activations_init_offsets(arrays) -%}
{% for array in arrays: -%}
  {# /* Check full array structure (size) and if it is not const */ #}
  {%- if array['size'] and not array['is_const'] -%}
    {%- if array['offset'] is none -%}
    {{ array['name'] }}.data = AI_PTR(NULL);
    {{ array['name'] }}.data_start = AI_PTR(NULL);
    {% else -%}
    {{ array['name'] }}.data = AI_PTR(activations + {{ array['offset'] }});
    {{ array['name'] }}.data_start = AI_PTR(activations + {{ array['start_offset'] }});
    {% endif -%}
  {% endif -%}
{%- endfor %}
{%- endmacro -%}


{# Custom layers Section #}

{% macro define_binary_operator(op) -%}
{% set params = op.tags %}
{{layers.declare_tensor_chain(op.tensor_chain)}}
AI_STATIC ai_operator_{{params.c_op_postfix}} {{op.name}}_op = AI_OPERATOR_INIT(
{{params.c_op_type.upper()}}, NULL, &{{params.next}}, {{params.c_op_compute}},
.tensors= &{{op.tensor_chain['name']}},
{% for param_str in params.param_strings: -%}
{{ param_str }},{{" "-}}
{%- endfor -%}
);
{%- endmacro -%}


{% macro include_custom(functions) -%}
{%- if functions -%}
#include "op_binary_func_custom.h"
{%- endif -%}
{%- endmacro -%}

{% macro custom_func_head(graph) -%}
AI_INTERNAL_API
ai_float ai_{{graph.name}}_fn(ai_float {{graph.input[0]}}, ai_float {{graph.input[1] or 'def_val'}})
{%- endmacro -%}

{% macro custom_func_body(graph) -%}
{{custom_func_head(graph)}}
{
  /* Custom generated code for {{graph.name}} */
{%- for constant in graph.const: %}
  AI_STATIC_CONST ai_float {{constant.name}} = {{constant.float_data}};
{%- endfor %}
{% for line in graph.body %}
  {{line}}
{%- endfor %}

  return {{graph.output}};
}
{%- endmacro -%}
