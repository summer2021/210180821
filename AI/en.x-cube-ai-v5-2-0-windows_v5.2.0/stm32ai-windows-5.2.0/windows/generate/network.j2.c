{%- import 'network_layers.j2.c' as layers -%}
{% set net_name = config['net_name'].lower() -%}
{% set NET_NAME = config['net_name'].upper() -%}

/**
  ******************************************************************************
  * @file    {{ net_name }}.c
  * @author  AST Embedded Analytics Research Platform
  * @date    {{ config['date_time'] }}
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

{{layers.include_custom(config[layers.FUNCTIONS])}}
#include "{{ net_name }}.h"

#include "ai_platform_interface.h"
#include "ai_math_helpers.h"

#include "core_common.h"
#include "layers.h"

{% if config['reloc']  %}
#include "ai_reloc_network.h"
{% endif %}

#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#define AI_TOOLS_VERSION_MAJOR {{ config['tools_version']['major'] }}
#define AI_TOOLS_VERSION_MINOR {{ config['tools_version']['minor'] }}
#define AI_TOOLS_VERSION_MICRO {{ config['tools_version']['micro'] }}
{% if config['ai_tools_version_extra'] -%}
#define AI_TOOLS_VERSION_EXTRA "{{ config['tools_version']['extra'] }}"
{% endif %}

#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#define AI_TOOLS_API_VERSION_MAJOR {{ config['tools_api_version']['major'] }}
#define AI_TOOLS_API_VERSION_MINOR {{ config['tools_api_version']['minor'] }}
#define AI_TOOLS_API_VERSION_MICRO {{ config['tools_api_version']['micro'] }}

#undef AI_NET_OBJ_INSTANCE
#define AI_NET_OBJ_INSTANCE g_{{ net_name }}
 
#undef AI_{{ NET_NAME }}_MODEL_SIGNATURE
#define AI_{{ NET_NAME }}_MODEL_SIGNATURE     "{{ config['model_signature'] }}"

#ifndef AI_TOOLS_REVISION_ID
#define AI_TOOLS_REVISION_ID     "{{ config['git_commit'] }}"
#endif

#undef AI_TOOLS_DATE_TIME
#define AI_TOOLS_DATE_TIME   "{{ config['date_time'] }}"

#undef AI_TOOLS_COMPILE_TIME
#define AI_TOOLS_COMPILE_TIME    __DATE__ " " __TIME__

#undef AI_{{ NET_NAME }}_N_BATCHES
#define AI_{{ NET_NAME }}_N_BATCHES         (1)

/**  Forward network declaration section  *************************************/
AI_STATIC ai_network AI_NET_OBJ_INSTANCE;


/**  Forward network array declarations  **************************************/
{% for array in config[layers.ARRAYS]: -%}
{{ layers.array_forward_declaration(array) }}   /* Array #{{ loop.index0 }} */
{% endfor %}

/**  Forward network tensor declarations  *************************************/
{% for tensor in config[layers.TENSORS]: -%}
{{ layers.tensor_forward_declaration(tensor) }}   /* Tensor #{{ loop.index0 }} */
{% endfor %}

/**  Forward network tensor chain declarations  *******************************/
{% for layer in config[layers.LAYERS]: -%}
{{ layers.tensor_chain_forward_declare(layer['tensor_chain']) }}   /* Chain #{{ loop.index0 }} */
{% endfor %}

/**  Forward network layer declarations  **************************************/
{% for layer in config[layers.LAYERS]: -%}
{{ layers.layer_forward_declaration(layer) }} /* Layer #{{ loop.index0 }} */
{% endfor %}

/**  Array declarations section  **********************************************/
{% for array in config[layers.ARRAYS]: -%}
{{ layers.declare_array(array, loop.index0) }}
{% endfor %}

{%- if config[layers.FORMATS]|length>0 -%}
/**  Array metadata declarations section  *************************************/
{% for format in config[layers.FORMATS]: -%}
{{ layers.declare_intq(format, loop.index0, is_const=True) }}
{% endfor %}
{%- endif -%}

/**  Tensor declarations section  *********************************************/
{% for tensor in config[layers.TENSORS]: -%}
{{ layers.declare_tensor(tensor, loop.index0) }}
{% endfor %}

/**  Layer declarations section  **********************************************/

{% for layer in config[layers.LAYERS]: -%}
{{"\n"-}}
  {{ layers.declare_layer(layer, 'AI_NET_OBJ_INSTANCE') }}
{% endfor %}

AI_NETWORK_OBJ_DECLARE(
  AI_NET_OBJ_INSTANCE, AI_STATIC,
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, {{ config['WeightSize'] }}, 1,
                     NULL),
  AI_BUFFER_OBJ_INIT(AI_BUFFER_FORMAT_U8,
                     1, 1, {{ config['MaxActivationSize'] }}, 1,
                     NULL),
  AI_TENSOR_LIST_IO_OBJ_INIT(AI_FLAG_NONE, AI_{{ NET_NAME }}_IN_NUM, {{ config['net_in_names'] }}),
  AI_TENSOR_LIST_IO_OBJ_INIT(AI_FLAG_NONE, AI_{{ NET_NAME }}_OUT_NUM, {{ config['net_out_names'] }}),
  &{{ config['first_layer'] }}, {{ config['net_signature'] }}, NULL)


{% if config['MaxActivationSize'] > 0 %}
AI_DECLARE_STATIC
ai_bool {{ net_name }}_configure_activations(
  ai_network* net_ctx, const ai_buffer* activation_buffer)
{
  AI_ASSERT(net_ctx &&  activation_buffer && activation_buffer->data)

  ai_ptr activations = AI_PTR(AI_PTR_ALIGN(activation_buffer->data, AI_{{ NET_NAME }}_ACTIVATIONS_ALIGNMENT));
  AI_ASSERT(activations)
  AI_UNUSED(net_ctx)

  {
    /* Updating activations (byte) offsets */
    {{ layers.activations_init_offsets(config[layers.ARRAYS]) }}
  }
  return true;
}
{% endif %}

{% if config['WeightSize'] > 0 %}
AI_DECLARE_STATIC
ai_bool {{ net_name }}_configure_weights(
  ai_network* net_ctx, const ai_buffer* weights_buffer)
{
  AI_ASSERT(net_ctx &&  weights_buffer && weights_buffer->data)

  ai_ptr weights = AI_PTR(weights_buffer->data);
  AI_ASSERT(weights)
  AI_UNUSED(net_ctx)

  {
    /* Updating weights (byte) offsets */
    {{ layers.weights_init_offsets(config[layers.ARRAYS]) }}
  }

  return true;
}
{% endif %}

/**  PUBLIC APIs SECTION  *****************************************************/

AI_API_ENTRY
ai_bool ai_{{ net_name }}_get_info(
  ai_handle network, ai_network_report* report)
{
  ai_network* net_ctx = AI_NETWORK_ACQUIRE_CTX(network);

  if ( report && net_ctx )
  {
    ai_network_report r = {
      .model_name        = AI_{{ NET_NAME }}_MODEL_NAME,
      .model_signature   = AI_{{ NET_NAME }}_MODEL_SIGNATURE,
      .model_datetime    = AI_TOOLS_DATE_TIME,
      
      .compile_datetime  = AI_TOOLS_COMPILE_TIME,
      
      .runtime_revision  = ai_platform_runtime_get_revision(),
      .runtime_version   = ai_platform_runtime_get_version(),

      .tool_revision     = AI_TOOLS_REVISION_ID,
      .tool_version      = {AI_TOOLS_VERSION_MAJOR, AI_TOOLS_VERSION_MINOR,
                            AI_TOOLS_VERSION_MICRO, 0x0},
      .tool_api_version  = {AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR,
                            AI_TOOLS_API_VERSION_MICRO, 0x0},

      .api_version            = ai_platform_api_get_version(),
      .interface_api_version  = ai_platform_interface_api_get_version(),
      
      .n_macc            = {{ config['Macc'] }},
      .n_inputs          = 0,
      .inputs            = NULL,
      .n_outputs         = 0,
      .outputs           = NULL,
      .activations       = AI_STRUCT_INIT,
      .params            = AI_STRUCT_INIT,
      .n_nodes           = 0,
      .signature         = 0x0,
    };

    if ( !ai_platform_api_get_network_report(network, &r) ) return false;

    *report = r;
    return true;
  }

  return false;
}

AI_API_ENTRY
ai_error ai_{{ net_name }}_get_error(ai_handle network)
{
  return ai_platform_network_get_error(network);
}

AI_API_ENTRY
ai_error ai_{{ net_name }}_create(
  ai_handle* network, const ai_buffer* network_config)
{
  return ai_platform_network_create(
    network, network_config, 
    &AI_NET_OBJ_INSTANCE,
    AI_TOOLS_API_VERSION_MAJOR, AI_TOOLS_API_VERSION_MINOR, AI_TOOLS_API_VERSION_MICRO);
}

AI_API_ENTRY
ai_handle ai_{{ net_name }}_destroy(ai_handle network)
{
  return ai_platform_network_destroy(network);
}

AI_API_ENTRY
ai_bool ai_{{ net_name }}_init(
  ai_handle network, const ai_network_params* params)
{
  ai_network* net_ctx = ai_platform_network_init(network, params);
  if ( !net_ctx ) return false;

  ai_bool ok = true;
  {%- if config['WeightSize'] > 0 %}
  ok &= {{ net_name }}_configure_weights(net_ctx, &params->params);
  {%- endif %}
  {%- if config['MaxActivationSize'] != 0 %}
  ok &= {{net_name}}_configure_activations(net_ctx, &params->activations);
  {%- endif %}

  ok &= ai_platform_network_post_init(network);

  return ok;
}


AI_API_ENTRY
ai_i32 ai_{{ net_name }}_run(
  ai_handle network, const ai_buffer* input, ai_buffer* output)
{
  return ai_platform_network_process(network, input, output);
}

AI_API_ENTRY
ai_i32 ai_{{ net_name }}_forward(ai_handle network, const ai_buffer* input)
{
  return ai_platform_network_process(network, input, NULL);
}

{% if config['reloc']  %}
AI_RELOC_NETWORK();
{% endif %}


#undef AI_{{ NET_NAME }}_MODEL_SIGNATURE
#undef AI_NET_OBJ_INSTANCE
#undef AI_TOOLS_VERSION_MAJOR
#undef AI_TOOLS_VERSION_MINOR
#undef AI_TOOLS_VERSION_MICRO
#undef AI_TOOLS_API_VERSION_MAJOR
#undef AI_TOOLS_API_VERSION_MINOR
#undef AI_TOOLS_API_VERSION_MICRO
#undef AI_TOOLS_DATE_TIME
#undef AI_TOOLS_COMPILE_TIME

{{ "" }}
