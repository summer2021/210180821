{% set data_module = config['data_module'].lower() -%}
{% set DATA_MODULE = config['data_module'].upper() -%}

/**
  ******************************************************************************
  * @file    {{ data_module }}.h
  * @author  AST Embedded Analytics Research Platform
  * @date    {{ config['date_time'] }}
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#ifndef __{{ DATA_MODULE }}_H_
#define __{{ DATA_MODULE }}_H_
#pragma once

#include "ai_platform.h"

#define AI_{{ DATA_MODULE }}_CONFIG           AI_HANDLE_NULL

#define AI_{{ DATA_MODULE }}_ACTIVATIONS_SIZE     ({{ config['MaxActivationSize'] }})

#define AI_{{ DATA_MODULE }}_WEIGHTS_SIZE         ({{ config['WeightSize'] }})

#define AI_{{ DATA_MODULE }}_ACTIVATIONS(ptr_)  \
  AI_BUFFER_OBJ_INIT( \
    AI_BUFFER_FORMAT_U8, \
    1, 1, AI_{{ DATA_MODULE }}_ACTIVATIONS_SIZE, 1, \
    AI_HANDLE_PTR(ptr_) )

#define AI_{{ DATA_MODULE }}_WEIGHTS(ptr_)  \
  AI_BUFFER_OBJ_INIT( \
    AI_BUFFER_FORMAT_U8|AI_BUFFER_FMT_FLAG_CONST, \
    1, 1, AI_{{ DATA_MODULE }}_WEIGHTS_SIZE, 1, \
    AI_HANDLE_PTR(ptr_) )


AI_API_DECLARE_BEGIN

/*!
 * @brief Get network weights array pointer as a handle ptr.
 * @ingroup {{ data_module }}
 * @return a ai_handle pointer to the weights array
 */
AI_API_ENTRY
ai_handle ai_{{ data_module }}_weights_get(void);


AI_API_DECLARE_END

#endif /* __{{ DATA_MODULE }}_H_ */
{{ "" }}

