{%- import 'network_layers.j2.c' as layers -%}
{% set net_name = config['net_name'].lower() -%}

/**
  ******************************************************************************
  * @file    op_binary_func_custom.h
  * @author  AST Embedded Analytics Research Platform
  * @date    {{ config['date_time'] }}
  * @brief   AI Tool Automatic Code Generator for Embedded NN computing
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#ifndef __OP_BINARY_FUNC_CUSTOM_H__
#define __OP_BINARY_FUNC_CUSTOM_H__
#pragma once

#include "ai_datatypes_defines.h"

AI_API_DECLARE_BEGIN
{% for graph in config[layers.FUNCTIONS]: %}
{{layers.custom_func_head(graph)}};
{% endfor %}
AI_API_DECLARE_END

#endif /* __OP_BINARY_FUNC_CUSTOM_H__ */

